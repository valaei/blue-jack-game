﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Image border1, border2, border3;
    bool scene0;
    

    void Awake() 
    {
        //DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (scene0)
        {
            StartCoroutine(AnimateBorder());
            if(PlayerPrefs.HasKey("LastHighScore"))
            {
                GameObject.Find("Score").GetComponent<Text>().text = PlayerPrefs.GetInt("LastHighScore").ToString();
                float time = PlayerPrefs.GetFloat("LastTime");
                int minutes = Mathf.FloorToInt(time / 60F);
                int seconds = Mathf.FloorToInt(time % 60F);
                int milliseconds = Mathf.FloorToInt((time * 100F) % 100F);
                GameObject.Find("Time").GetComponent<Text>().text = minutes.ToString ("00") + ":" + seconds.ToString ("00") + ":" + milliseconds.ToString("00");
            }
            else
            {
                PlayerPrefs.SetInt("LastHighScore", 0);
                PlayerPrefs.SetFloat("LastTime", 0f);
                PlayerPrefs.Save();
            }
        }  
    }

    // Update is called once per frame
    void Update()
    {
         
    }

    public void LoadFirstLevel()
    {
        StopCoroutine(AnimateBorder());
        SceneManager.LoadScene(1);
        //SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public void LoadMainScene()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    IEnumerator AnimateBorder()
    {
        while (true)
        {
            border3.enabled = false;
            yield return new WaitForSeconds(0.15f);
            border3.enabled = true;
            yield return new WaitForSeconds(0.15f);
            border2.enabled = false;
            yield return new WaitForSeconds(0.15f);
            border2.enabled = true;
            yield return new WaitForSeconds(0.15f);
            border1.enabled = false;
            yield return new WaitForSeconds(0.15f);
            border1.enabled = true;            
        }
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.buildIndex == 0)
        {
            scene0 = true;
        }
    }
}
