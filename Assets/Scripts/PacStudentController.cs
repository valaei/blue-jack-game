﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PacStudentController : MonoBehaviour
{
    string lastInput, currentInput; 
    int[,] fullMap;
    public (int, int) currentPosition, nextPosition;
    GameObject blueJack;
    Tweener tweener;
    const float walkSize = 1.28f;
    Vector3 nextCoordinates;
    bool isMoving, collision, spidyTimeStart, dying, win, spidyDead;
    public bool start;
    Animator animatorController;
    public AudioSource eatSource, walkSource, collisionSource;
    ParticleSystem dust;
    Text score, SpidyTime, gameTime;
    int newScore, life, pellets;
    float timer;
    GameObject spidy1, spidy2, spidy3, spidy4;
    Coroutine spidy1C, spidy2C,spidy3C, spidy4C;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Animator>().enabled = false;
        fullMap = Camera.main.GetComponent<LevelGenerator>().fullMap;
        blueJack = Camera.main.GetComponent<LevelGenerator>().blueJack;
        tweener = Camera.main.GetComponent<Tweener>();
        animatorController = blueJack.GetComponent<Animator>(); 
        dust = blueJack.GetComponent<ParticleSystem>();
        collisionSource = gameObject.GetComponents<AudioSource>()[2];
        eatSource = gameObject.GetComponents<AudioSource>()[1];
        walkSource = gameObject.GetComponents<AudioSource>()[0];  
        score = GameObject.Find("Score").GetComponent<Text>();
        SpidyTime = GameObject.Find("SpidyTime").GetComponent<Text>();
        spidy1 = GameObject.FindWithTag("Spidy1");
        spidy2 = GameObject.FindWithTag("Spidy2");
        spidy3 = GameObject.FindWithTag("Spidy3");
        spidy4 = GameObject.FindWithTag("Spidy4");
        nextCoordinates = blueJack.transform.position;
        currentPosition = (2, 2);
        isMoving = false;
        collision = false;
        start = false;
        spidyTimeStart = false;
        dying = false;
        win = false;
        spidyDead = false;
        newScore = 0;
        life = 3;
        pellets = 221;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(pellets == 0)
        {
            win = true;
            StartCoroutine(GameOver());
        }
        else if(start)
        {
            if (Input.GetKey("a"))
            {
                lastInput = "a";
            }
            else if (Input.GetKey("w"))
            {
                lastInput = "w";
            }
            else if (Input.GetKey("d"))
            {
                lastInput = "d";
            }
            else if (Input.GetKey("s"))
            {
                lastInput = "s"; 
            }
            MovePacStudent();
        }
        if(spidyTimeStart)
        {
            float time;
            if(timer <= 10)
            {
                time = 10 - timer;
                int minutes = Mathf.FloorToInt(time / 60F);
                int seconds = Mathf.FloorToInt(time % 60F);
                int milliseconds = Mathf.FloorToInt((time * 100F) % 100F);
                SpidyTime.text = minutes.ToString ("00") + ":" + seconds.ToString ("00") + ":" + milliseconds.ToString("00");
            }
            else 
            {
                spidyTimeStart = false;
                SpidyTime.text = "";
                if(!spidyDead)
                {
                    Camera.main.GetComponent<LevelGenerator>().ChangeBackgroundMusic(0);
                }
            }
        }
    }

    void MovePacStudent()
    {
        isMoving = tweener.TweenExists(blueJack.transform) ? true : false;
        if(lastInput != null && !isMoving && !dying)
        {
            if (IsWalkable(lastInput))
            {
                ChangeAnim();
                collision = false;
                currentInput = lastInput;
                tweener.AddTween(blueJack.transform, blueJack.transform.position, nextCoordinates, 0.2f);
                currentPosition = nextPosition;
            }
            else if(IsWalkable(currentInput))
            {
                collision = false;
                tweener.AddTween(blueJack.transform, blueJack.transform.position, nextCoordinates, 0.2f);
                currentPosition = nextPosition;
            }
            else if(nextPosition == (15, 0))
            {
                currentPosition = (15, 27);
                tweener.AddTween(blueJack.transform,  new Vector3(27 * walkSize, 14 * walkSize, -0.2f),  new Vector3(26 * walkSize, 14 * walkSize, -0.2f), 0.2f);
            }
            else if(nextPosition == (15, 29))
            {
                currentPosition = (15, 2);
                tweener.AddTween(blueJack.transform,  new Vector3(0 * walkSize, 14 * walkSize, -0.2f),  new Vector3(1 * walkSize, 14 * walkSize, -0.2f), 0.2f);
            }
            else if(!collision && currentInput != null)
            {
                collision = true;
                collisionSource.Play();
            }
        }
    }

    bool IsWalkable(string input)
    {
        if(input == "a")
        {
            nextPosition = (currentPosition.Item1, currentPosition.Item2 - 1);
        }
        else if(input == "w")
        {
            nextPosition = (currentPosition.Item1 - 1, currentPosition.Item2);
        }
        else if(input == "d")
        {
            nextPosition = (currentPosition.Item1, currentPosition.Item2 + 1);
        }
        else if(input == "s")
        {
            nextPosition = (currentPosition.Item1 + 1, currentPosition.Item2);
        }
        if(nextPosition.Item2 == 0 || nextPosition.Item2 == 29)
        {
            dust.Play();
            walkSource.Play();
            return false;
        }
        if(fullMap[nextPosition.Item1, nextPosition.Item2] == 0)
        {
            dust.Play();
            nextCoordinates.x = (nextPosition.Item2 - 1) * walkSize;
            nextCoordinates.y = (29 - nextPosition.Item1) * walkSize;
            nextCoordinates.z = 0.0f;
            walkSource.Play();
            return true;
        }
        else if(fullMap[nextPosition.Item1, nextPosition.Item2] == 5 ||
                fullMap[nextPosition.Item1, nextPosition.Item2] == 6)
        {
            dust.Play();
            nextCoordinates.x = (nextPosition.Item2 - 1) * walkSize;
            nextCoordinates.y = (29 - nextPosition.Item1) * walkSize;
            nextCoordinates.z = 0.0f;
            eatSource.Play();
            return true;
        }
        return false;
    }

    void ChangeAnim()
    {
        if (currentInput != null)
        {
            animatorController.ResetTrigger(currentInput);
        }
        animatorController.SetTrigger(lastInput);
    }


    void OnTriggerEnter2D (Collider2D col) 
    { 
        if(col.gameObject.tag == "NormalPellet")  
        {
            pellets--;
            Destroy(col.gameObject);
            fullMap[currentPosition.Item1, currentPosition.Item2] = 0;
            newScore += 10;
            score.text = "Score : " + newScore;
        }
        else if(col.gameObject.tag == "PowerPellet")
        {
            pellets--;
            Destroy(col.gameObject);

            if(pellets != 0)
            {
                fullMap[currentPosition.Item1, currentPosition.Item2] = 0;
                timer = 0;
                if(spidyTimeStart)
                {
                    StopCoroutine(spidy1C);
                    StopCoroutine(spidy2C);
                    StopCoroutine(spidy3C);
                    StopCoroutine(spidy4C);  
                }
                else if(!spidyDead)
                {
                    Camera.main.GetComponent<LevelGenerator>().ChangeBackgroundMusic(1);
                }
                spidy1C = StartCoroutine(SpidyCountDown(spidy1));
                spidy2C = StartCoroutine(SpidyCountDown(spidy2));
                spidy3C = StartCoroutine(SpidyCountDown(spidy3));
                spidy4C = StartCoroutine(SpidyCountDown(spidy4));
                spidyTimeStart = true;
            }
        }
        else if(col.gameObject.tag == "Bonus")
        {
            col.gameObject.SetActive(false);
            newScore += 100;
            score.text = "Score : " + newScore;            
            eatSource.Play();
        }
        else
        {
            if(!dying && col.gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsTag("Walking"))
            {
                dying = true;
                var main = dust.main;
                main.gravityModifier = -2f;
                main.startSizeXMultiplier = 1.5f;
                main.startSizeYMultiplier = 1.5f;
                dust.Play();
                StartCoroutine(GameOver());
            }
            else if(col.gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Spidy-Scared") ||
                    col.gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Spidy-Recovery"))
            {
                newScore += 300;
                Camera.main.GetComponent<LevelGenerator>().ChangeBackgroundMusic(2);
                
                if(col.gameObject.tag == "Spidy1")
                {
                    StopCoroutine(spidy1C);
                    StartCoroutine(KillSpidy(spidy1));
                }
                else if(col.gameObject.tag == "Spidy2")
                {
                    StopCoroutine(spidy2C);
                    StartCoroutine(KillSpidy(spidy2));
                }
                else if(col.gameObject.tag == "Spidy3")
                {
                    StopCoroutine(spidy3C);
                    StartCoroutine(KillSpidy(spidy3));
                }
                else
                {
                    StopCoroutine(spidy4C);
                    StartCoroutine(KillSpidy(spidy4));                  
                }
            }
        }
    }
    IEnumerator SpidyCountDown(GameObject spidy)
    {
        spidy.GetComponent<Animator>().ResetTrigger("s");
        spidy.GetComponent<Animator>().ResetTrigger("wa");
        spidy.GetComponent<Animator>().ResetTrigger("re");
        spidy.GetComponent<Animator>().ResetTrigger("de");
        spidy.GetComponent<Animator>().SetTrigger("sc");
        yield return new WaitForSeconds(7f);
        spidy.GetComponent<Animator>().ResetTrigger("sc");
        spidy.GetComponent<Animator>().SetTrigger("re");
        yield return new WaitForSeconds(3f);
        spidy.GetComponent<Animator>().ResetTrigger("re");
        spidy.GetComponent<Animator>().SetTrigger("wa");
        spidy.GetComponent<Animator>().SetTrigger("s");

    }
    IEnumerator KillSpidy(GameObject spidy)
    {
        spidyDead = true;
        Camera.main.GetComponent<LevelGenerator>().spidyDead = true;
        spidy.GetComponent<Animator>().ResetTrigger("s");
        spidy.GetComponent<Animator>().ResetTrigger("wa");
        spidy.GetComponent<Animator>().ResetTrigger("re");
        spidy.GetComponent<Animator>().SetTrigger("de");
        yield return new WaitForSeconds(2f);
        spidy.GetComponent<Animator>().ResetTrigger("de");
        spidy.GetComponent<Animator>().SetTrigger("s");
        if(Camera.main.GetComponent<LevelGenerator>().spidyDead)
        {
            if(spidyTimeStart)
            {
                Camera.main.GetComponent<LevelGenerator>().ChangeBackgroundMusic(1);
            }
            else
            {
                Camera.main.GetComponent<LevelGenerator>().ChangeBackgroundMusic(0);
            }
        }
        spidyDead = false;
    }
    
    IEnumerator GameOver()
    {
        currentInput = null;
        lastInput = null;
        if(dying)
        {
            if(life != 0)
            {
                GameObject.Find("Life" + life).SetActive(false);
                life -= 1;
            }
        }
        gameObject.GetComponent<Animator>().ResetTrigger("d");
        gameObject.GetComponent<Animator>().ResetTrigger("a");
        gameObject.GetComponent<Animator>().ResetTrigger("s");
        gameObject.GetComponent<Animator>().ResetTrigger("w");
        gameObject.GetComponent<Animator>().SetTrigger("de");
        yield return new WaitForSeconds(1.6f);
        var main = dust.main;
        main.gravityModifier = 0f;
        main.startSizeXMultiplier = 0.2f;
        main.startSizeYMultiplier = 0.2f;
        gameObject.GetComponent<Animator>().SetTrigger("d");
        if(life == 0 || win)
        {
            start = false;
            Camera.main.GetComponent<LevelGenerator>().start=false;
            Text countDown = GameObject.Find("CountDown").GetComponent<Text>();
            countDown.fontSize = 30;
            countDown.text = "Game Over";
            gameObject.GetComponent<Renderer>().enabled = false;
            float newTime = Camera.main.GetComponent<LevelGenerator>().timer;
            int lastHighScore = PlayerPrefs.GetInt("LastHighScore");
            float lastTime = PlayerPrefs.GetFloat("LastTime");
            if(newScore > lastHighScore || (newScore == lastHighScore && newTime < lastTime))
            {
                PlayerPrefs.SetInt("LastHighScore", newScore);
                PlayerPrefs.SetFloat("LastTime", newTime);
                PlayerPrefs.Save();
            }
            yield return new WaitForSeconds(3f);
            GameObject.Find("UIManager").GetComponent<UIManager>().LoadMainScene();
        }
        gameObject.transform.position = new Vector3(1 * 1.28f, 27 * 1.28f, 0f); 
        dying = false;
        currentPosition = (2, 2);
    }

 
}
