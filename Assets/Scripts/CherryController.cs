﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CherryController : MonoBehaviour
{
    GameObject bonus;
    Tweener tweener;


    // Start is called before the first frame update
    void Start()
    {
        bonus = Camera.main.GetComponent<LevelGenerator>().newBonus;
        tweener = Camera.main.GetComponent<Tweener>();
        Vector3 center = new Vector3(13.5f * 1.28f, 14 * 1.28f, 0f);
        Vector3 endPosition = center + (center - bonus.transform.position);
        tweener.AddTween(bonus.transform, bonus.transform.position, endPosition, 6f);
    }

    // Update is called once per frame
    void Update()
    {
        if(bonus != null)
        {
            if(!tweener.TweenExists(bonus.transform))
            {
                Destroy(gameObject);
            }
        }
    }
}
