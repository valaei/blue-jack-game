﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpidyController : MonoBehaviour
{
    int[,] fullMap;
    string lastInput, currentInput;
    bool isMoving, spidy4FindLoc;
    (int, int) currentPosition, nextPosition, startPosition, spidy4RandomLocation;
    Tweener tweener;
    const float walkSize = 1.28f;
    Vector3 nextCoordinates;
    public bool start;
    Animator animatorController;
    GameObject spidy;
    int startMove, spidy4LocIndex;
    string[] spidy1Start = {"d", "d", "w", "w"}; 
    string[] spidy2Start = {"d", "d", "s", "s"}; 
    string[] spidy3Start = {"a", "a", "w", "w"}; 
    string[] spidy4Start = {"a", "a", "s", "s"};
    string[] moves = {"w", "d", "s", "a"};
    List<(int, int)> spidy4Path;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Animator>().enabled = false;
        fullMap = Camera.main.GetComponent<LevelGenerator>().fullMap;
        tweener = Camera.main.GetComponent<Tweener>();
        animatorController = gameObject.GetComponent<Animator>(); 
        isMoving = false;
        start = false;
        currentInput = "s";
        startMove = 0;
        spidy4FindLoc = false;
        spidy4RandomLocation = (-1, -1);
        if(gameObject.tag == "Spidy1")
        {
            currentPosition = (14, 12);
            startPosition = (14, 12);
        }
        else if(gameObject.tag == "Spidy2")
        {
            currentPosition = (16, 12);
            startPosition = (16, 12);

        }
        else if(gameObject.tag == "Spidy3")
        {
            currentPosition = (14, 17);
            startPosition = (14, 17);
        }
        else
        {
            currentPosition = (16, 17);
            startPosition = (16, 17);
        }
        spidy4Path = new List<(int, int)>();
        CreateSpidy4Path();
    }

    // Update is called once per frame
    void Update()
    {
        if(start)
        {
            if(animatorController.GetCurrentAnimatorStateInfo(0).IsName("Spidy-Dead"))
            {
                tweener.AddTween(gameObject.transform,  gameObject.transform.position,  new Vector3((startPosition.Item2 - 1) * walkSize, (29 - startPosition.Item1) * walkSize, 0), 2f);
                currentPosition = startPosition;
                startMove = 0;
            }
            else
            {
                MoveSpidy();
            }
        }
    }
    void MoveSpidy()
    {
        isMoving = tweener.TweenExists(gameObject.transform) ? true : false;
        if(!isMoving)
        {
            WalkAlgorithm();
            if (lastInput != null && IsWalkable(lastInput))
            {
                nextCoordinates.x = (nextPosition.Item2 - 1) * walkSize;
                nextCoordinates.y = (29 - nextPosition.Item1) * walkSize;
                nextCoordinates.z = 0.0f;
                if(lastInput != currentInput)
                {
                    currentInput = lastInput;
                    ChangeAnim();
                }
                tweener.AddTween(gameObject.transform, gameObject.transform.position, nextCoordinates, 0.2f);
                currentPosition = nextPosition;
                if(gameObject.tag == "Spidy4" && spidy4FindLoc == false && spidy4Path.Contains(nextPosition))
                {
                    spidy4LocIndex = spidy4Path.IndexOf(nextPosition);
                    spidy4FindLoc = true;        
                }
            }
            else if(nextPosition == (15, 0))
            {
                currentPosition = (15, 27);
                tweener.AddTween(gameObject.transform,  new Vector3(27 * walkSize, 14 * walkSize, -0.2f),  new Vector3(26 * walkSize, 14 * walkSize, 0), 0.2f);
            }
            else if(nextPosition == (15, 29))
            {
                currentPosition = (15, 2);
                tweener.AddTween(gameObject.transform,  new Vector3(0 * walkSize, 14 * walkSize, -0.2f),  new Vector3(1 * walkSize, 14 * walkSize, 0), 0.2f);
            }
        }
    }

    void WalkAlgorithm()
    {
        if(startMove < 4)
        {
            if(gameObject.tag == "Spidy1")
            {
                lastInput = spidy1Start[startMove];
            }
            else if(gameObject.tag == "Spidy2")
            {
                lastInput = spidy2Start[startMove];
            }
            else if(gameObject.tag == "Spidy3")
            {
                lastInput = spidy3Start[startMove];
            }
            else
            {
                lastInput = spidy4Start[startMove];
            }
            startMove++;
        }
        else
        {
            if(gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Spidy-Scared") ||
               gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Spidy-Recovery"))
            {
                Spidy1AI();
                spidy4FindLoc = false;
                spidy4RandomLocation = (-1, -1);
            }
            else
            {
                if(gameObject.tag == "Spidy1")
                {
                    Spidy1AI();
                }
                else if(gameObject.tag == "Spidy2")
                {
                    Spidy2AI();
                }
                else if(gameObject.tag == "Spidy3")
                {
                    Spidy3AI();
                }
                else
                {
                    if(spidy4RandomLocation == (-1, -1))
                    {
                        int shortestPath = 1000;
                        for (int i = 0; i < spidy4Path.Count; i++)
                        {
                            int distance = Mathf.Abs(currentPosition.Item1 - spidy4Path[i].Item1) + Mathf.Abs(currentPosition.Item2 - spidy4Path[i].Item2);
                            if(distance < shortestPath)
                            {
                                shortestPath = distance;
                                spidy4RandomLocation = spidy4Path[i];
                                spidy4LocIndex = i;
                            }
                        }
                        spidy4FindLoc = false;
                    }
                    Spidy4AI();
                }
            }
        }
    }
   
    void Spidy1AI()
    {
        string random;
        string illegalMove = moves[(Array.IndexOf(moves, currentInput) + 2) % 4];
        List<string> legalMoves = new List<string>();
        for(int i = 0; i < 4; i++)
        {
            if(moves[i] != illegalMove)
            {
                legalMoves.Add(moves[i]);
            }
        }
        (int, int) blueJackPos = GameObject.Find("BlueJack(Clone)").GetComponent<PacStudentController>().currentPosition;
        List<string> finalMoves = new List<string>();
        int currentDistance = Mathf.Abs(currentPosition.Item1 - blueJackPos.Item1) + Mathf.Abs(currentPosition.Item2 - blueJackPos.Item2);
        for(int i = 0; i < legalMoves.Count; i++)
        {
            if(IsWalkable(legalMoves[i]))
            {
                if(nextPosition == (15, 0))
                {
                    nextPosition = (15, 27);
                }
                else if(nextPosition == (15, 29))
                {
                    nextPosition = (15, 2);
                }
                int nextDistance = Mathf.Abs(nextPosition.Item1 - blueJackPos.Item1) + Mathf.Abs(nextPosition.Item2 - blueJackPos.Item2);
                if(nextDistance >= currentDistance)
                {
                    finalMoves.Add(legalMoves[i]);
                }
            }
        }
        if(finalMoves.Count != 0)
        {
            random = finalMoves[UnityEngine.Random.Range(0, finalMoves.Count)];
        }
        else
        {
            random = legalMoves[UnityEngine.Random.Range(0, legalMoves.Count)];
        }
        lastInput = random;
    }
   
    void Spidy2AI()
    {
        string random;
        string illegalMove = moves[(Array.IndexOf(moves, currentInput) + 2) % 4];
        List<string> legalMoves = new List<string>();
        for(int i = 0; i < 4; i++)
        {
            if(moves[i] != illegalMove)
            {
                legalMoves.Add(moves[i]);
            }
        }
        (int, int) blueJackPos = GameObject.Find("BlueJack(Clone)").GetComponent<PacStudentController>().currentPosition;
        List<string> finalMoves = new List<string>();
        int currentDistance = Mathf.Abs(currentPosition.Item1 - blueJackPos.Item1) + Mathf.Abs(currentPosition.Item2 - blueJackPos.Item2);
        for(int i = 0; i < legalMoves.Count; i++)
        {
            if(IsWalkable(legalMoves[i]))
            {
                if(nextPosition == (15, 0))
                {
                    nextPosition = (15, 27);
                }
                else if(nextPosition == (15, 29))
                {
                    nextPosition = (15, 2);
                }
                int nextDistance = Mathf.Abs(nextPosition.Item1 - blueJackPos.Item1) + Mathf.Abs(nextPosition.Item2 - blueJackPos.Item2);
                if(nextDistance <= currentDistance)
                {
                    finalMoves.Add(legalMoves[i]);
                }
            }
        }
        if(finalMoves.Count != 0)
        {
            random = finalMoves[UnityEngine.Random.Range(0, finalMoves.Count)];
        }
        else
        {
            random = legalMoves[UnityEngine.Random.Range(0, legalMoves.Count)];
        }
        lastInput = random;
    }
    
    void Spidy3AI()
    {
        string random;
        string illegalMove = moves[(Array.IndexOf(moves, currentInput) + 2) % 4];
        List<string> legalMoves = new List<string>();
        for(int i = 0; i < 4; i++)
        {
            if(moves[i] != illegalMove)
            {
                legalMoves.Add(moves[i]);
            }
        }
        random = legalMoves[UnityEngine.Random.Range(0, legalMoves.Count)];
        lastInput = random;
    }
   
    void Spidy4AI()
    {
        if(!spidy4FindLoc)
        {
            string random;
            string illegalMove = moves[(Array.IndexOf(moves, currentInput) + 2) % 4];
            List<string> legalMoves = new List<string>();
            for(int i = 0; i < 4; i++)
            {
                if(moves[i] != illegalMove)
                {
                    legalMoves.Add(moves[i]);
                }
            }
            List<string> finalMoves = new List<string>();
            int currentDistance = Mathf.Abs(currentPosition.Item1 - spidy4RandomLocation.Item1) + Mathf.Abs(currentPosition.Item2 - spidy4RandomLocation.Item2);
            for(int i = 0; i < legalMoves.Count; i++)
            {
                if(IsWalkable(legalMoves[i]))
                {
                    if(nextPosition == (15, 0))
                    {
                        nextPosition = (15, 27);
                    }
                    else if(nextPosition == (15, 29))
                    {
                        nextPosition = (15, 2);
                    }
                    int nextDistance = Mathf.Abs(nextPosition.Item1 - spidy4RandomLocation.Item1) + Mathf.Abs(nextPosition.Item2 - spidy4RandomLocation.Item2);
                    if(nextDistance < currentDistance)
                    {
                        finalMoves.Add(legalMoves[i]);
                    }
                }
            }
            if(finalMoves.Count != 0)
            {
                random = finalMoves[UnityEngine.Random.Range(0, finalMoves.Count)];
            }
            else
            {
                random = legalMoves[UnityEngine.Random.Range(0, legalMoves.Count)];
            }
            lastInput = random;
            if(currentDistance == 1)
            {
                spidy4FindLoc = true;        
            }
        }
        else 
        {
            spidy4LocIndex = (spidy4LocIndex + 1) % spidy4Path.Count;
            (int, int) next = spidy4Path[spidy4LocIndex];
            if(next.Item1 > currentPosition.Item1)
            {
                lastInput = "s";
            }
            else if(next.Item1 < currentPosition.Item1)
            {
                lastInput = "w";
            }
            else if(next.Item2 > currentPosition.Item2)
            {
                lastInput = "d";
            }
            else 
            {
                lastInput = "a";
            }
        }
    }

    bool IsWalkable(string input)
    {
        if(input == "a")
        {
            nextPosition = (currentPosition.Item1, currentPosition.Item2 - 1);
        }
        else if(input == "w")
        {
            nextPosition = (currentPosition.Item1 - 1, currentPosition.Item2);
        }
        else if(input == "d")
        {
            nextPosition = (currentPosition.Item1, currentPosition.Item2 + 1);
        }
        else if(input == "s")
        {
            nextPosition = (currentPosition.Item1 + 1, currentPosition.Item2);
        }
        if(nextPosition.Item2 == 0 || nextPosition.Item2 == 29 || 
          (startMove == 4 && (nextPosition == (13, 14) || nextPosition == (13, 15) || 
          nextPosition == (17, 14) || nextPosition == (17, 15))))
        {
            return false;
        }
        if(fullMap[nextPosition.Item1, nextPosition.Item2] == 0 ||
           fullMap[nextPosition.Item1, nextPosition.Item2] == 5 ||
           fullMap[nextPosition.Item1, nextPosition.Item2] == 6)
        {
            return true;
        }
        return false;
    }

    void ChangeAnim()
    {
        if (currentInput != null)
        {
            animatorController.ResetTrigger(currentInput);
        }
        animatorController.SetTrigger(lastInput);
    }

    // Stupid & Fast Solution! :D
    void CreateSpidy4Path()
    {
        for(int i = 2; i <= 13; i++)
        {
            spidy4Path.Add((2, i));
        }
        for(int i = 3; i <= 6; i++)
        {
            spidy4Path.Add((i, 13));
        }
        for(int i = 14; i <= 16; i++)
        {
            spidy4Path.Add((6, i));
        }
        for(int i = 5; i >= 2; i--)
        {
            spidy4Path.Add((i, 16));
        }
        for(int i = 17; i <= 27; i++)
        {
            spidy4Path.Add((2, i));
        }
        for(int i = 3; i <= 9; i++)
        {
            spidy4Path.Add((i, 27));
        }
        for(int i = 26; i >= 22; i--)
        {
            spidy4Path.Add((9, i));
        }
        for(int i = 10; i <= 21; i++)
        {
            spidy4Path.Add((i, 22));
        }
        for(int i = 23; i <= 27; i++)
        {
            spidy4Path.Add((21, i));
        }
        for(int i = 22; i <= 28; i++)
        {
            spidy4Path.Add((i, 27));
        }
        for(int i = 26; i >= 16; i--)
        {
            spidy4Path.Add((28, i));
        }
        for(int i = 28; i >= 24; i--)
        {
            spidy4Path.Add((i, 16));
        }
        for(int i = 15; i >= 13; i--)
        {
            spidy4Path.Add((24, i));
        }
        for(int i = 25; i <= 28; i++)
        {
            spidy4Path.Add((i, 13));
        }
        for(int i = 12; i >= 2; i--)
        {
            spidy4Path.Add((28, i));
        }
        for(int i = 27; i >= 21; i--)
        {
            spidy4Path.Add((i, 2));
        }
        for(int i = 3; i <= 7; i++)
        {
            spidy4Path.Add((21, i));
        }
        for(int i = 20; i >= 9; i--)
        {
            spidy4Path.Add((i, 7));
        }
        for(int i = 6; i >= 2; i--)
        {
            spidy4Path.Add((9, i));
        }
        for(int i = 8; i >= 3; i--)
        {
            spidy4Path.Add((i, 2));
        }
    }
}
