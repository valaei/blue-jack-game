﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LevelGenerator : MonoBehaviour
{

    private int[,] levelMap =
    {
        {1,2,2,2,2,2,2,2,2,2,2,2,2,7},
        {2,5,5,5,5,5,5,5,5,5,5,5,5,4},
        {2,5,3,4,4,3,5,3,4,4,4,3,5,4},
        {2,6,4,0,0,4,5,4,0,0,0,4,5,4},
        {2,5,3,4,4,3,5,3,4,4,4,3,5,3},
        {2,5,5,5,5,5,5,5,5,5,5,5,5,5},
        {2,5,3,4,4,3,5,3,3,5,3,4,4,4},
        {2,5,3,4,4,3,5,4,4,5,3,4,4,3},
        {2,5,5,5,5,5,5,4,4,5,5,5,5,4},
        {1,2,2,2,2,1,5,4,3,4,4,3,0,4},
        {0,0,0,0,0,2,5,4,3,4,4,3,0,3},
        {0,0,0,0,0,2,5,4,4,0,0,0,0,0},
        {0,0,0,0,0,2,5,4,4,0,3,4,4,0},
        {2,2,2,2,2,1,5,3,3,0,4,0,0,0},
        {0,0,0,0,0,0,5,0,0,0,4,0,0,0},
    };

    public int[,] fullMap { get; set; }

    public bool start, spidyDead;

    public GameObject lifeIndicator, bonus, normalPallet, powerPalllet, innerCorner, 
                      innerLine, outerCorner, outerLine, tLeft, tRight, spidy1, spidy2, spidy3, 
                      spidy4, blueJack, newBonus;

    public AudioSource bgSource;

    Text time, countDown;

    public float timer;

    int bgNum;


    void Awake()
    {
        start = false;
        GetFullMap(levelMap);
        InitiateMap();
        InitiateCharacters();
    }

    // Start is called before the first frame update
    void Start()
    {
        bgNum = 0;
        bgSource = gameObject.GetComponents<AudioSource>()[bgNum];
        time = GameObject.Find("Time").GetComponent<Text>();
        countDown = GameObject.Find("CountDown").GetComponent<Text>();
        StartCoroutine(CountDown());
    }

    // Update is called once per frame
    void Update()
    {
        if(start)
        {
            timer += Time.deltaTime;
            int minutes = Mathf.FloorToInt(timer / 60F);
            int seconds = Mathf.FloorToInt(timer % 60F);
            int milliseconds = Mathf.FloorToInt((timer * 100F) % 100F);
            time.text = "Time : " + minutes.ToString ("00") + ":" + seconds.ToString ("00") + ":" + milliseconds.ToString("00");
        }
    }

    void GetFullMap(int[,] levelMap) 
    {
        int levelMapRows = levelMap.GetLength(0);
        int levelMapCols = levelMap.GetLength(1);
        fullMap = new int[(levelMapRows * 2) + 1, (levelMapCols + 1) * 2];
        for (int i = 0; i < fullMap.GetLength(0); i++)
        {
            for (int j = 0; j < fullMap.GetLength(1); j++)
            {
                if (i == 0 || j == 0 || i == fullMap.GetLength(0) - 1 || j == fullMap.GetLength(1) - 1)
                    fullMap[i, j] = 0;
                else if (i - 1 < levelMapRows && j - 1 < levelMapCols)
                    fullMap[i, j] = levelMap [i - 1, j - 1];
                else if (i - 1 >= levelMapRows && j - 1 < levelMapCols)
                    fullMap[i, j] = levelMap[fullMap.GetLength(0) - i - 2, j - 1];
                else if (i - 1 < levelMapRows && j - 1 >= levelMapCols)
                    fullMap[i, j] = levelMap[i - 1, fullMap.GetLength(1) - j - 2];
                else
                    fullMap[i, j] = levelMap[fullMap.GetLength(0) - i - 2, fullMap.GetLength(1) - j - 2];
            }
        }
        fullMap[2, 2] = 0;
    }

    void InitiateMap()
    {
        GameObject wall = GameObject.FindGameObjectWithTag ("Wall");
        GameObject pallet = GameObject.FindGameObjectWithTag ("Pallet");

        int caseSwitch;
        for (int i = 0; i < fullMap.GetLength(0); i++)
        {
            for (int j = 0; j < fullMap.GetLength(1); j++)
            {
                caseSwitch = fullMap[i, j];
                switch (caseSwitch)
                {
                    case 0:
                        break;
                    case 1:
                        if (IsLine(i, j + 1) && IsLine(i + 1, j))
                            Instantiate(outerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 90)), wall.transform);
                        else if (IsLine(i - 1 , j) && IsLine(i, j + 1))
                            Instantiate(outerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 180)), wall.transform);
                        else if (IsLine(i , j - 1) && IsLine(i - 1 , j))
                            Instantiate(outerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 270)), wall.transform);
                        else
                            Instantiate(outerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.identity, wall.transform);
                        break;
                    case 2:
                        if(fullMap[i + 1, j] == 0)
                            Instantiate(outerLine, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 180)), wall.transform);
                        else if(fullMap[i, j - 1] == 0)
                            Instantiate(outerLine, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 90)), wall.transform);
                        else if(fullMap[i, j + 1] == 0)
                            Instantiate(outerLine, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 270)), wall.transform);
                        else
                            Instantiate(outerLine, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.identity, wall.transform);
                        break;
                    case 3:
                        if (IsLine(i, j + 1) && IsLine(i + 1, j) && IsLine(i - 1, j) && IsLine(i, j - 1))
                        {
                            if (fullMap[i + 1, j - 1] == 0 || fullMap[i + 1, j - 1] == 5)
                                Instantiate(innerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.identity, wall.transform);
                            else if (fullMap[i + 1, j + 1] == 0 || fullMap[i + 1, j + 1] == 5)
                                Instantiate(innerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 90)), wall.transform);
                            else if (fullMap[i - 1, j - 1] == 0 || fullMap[i - 1, j - 1] == 5)
                                Instantiate(innerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 270)), wall.transform);
                            else
                                Instantiate(innerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 180)), wall.transform);
                        }
                        else 
                        {
                            if (IsLine(i, j + 1) && IsLine(i + 1, j))
                                Instantiate(innerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 90)), wall.transform);
                            else if (IsLine(i - 1 , j) && IsLine(i, j + 1))
                                Instantiate(innerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 180)), wall.transform);
                            else if (IsLine(i , j - 1) && IsLine(i - 1 , j))
                                Instantiate(innerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 270)), wall.transform);
                            else
                                Instantiate(innerCorner, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.identity, wall.transform);
                        }
                        break;
                    case 4:
                        if (IsLine(i + 1 , j) && IsLine(i - 1, j))
                            Instantiate(innerLine, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 90)), wall.transform);
                        else
                            Instantiate(innerLine, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.identity, wall.transform);
                        break;
                    case 5:
                        Instantiate(normalPallet, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.identity, pallet.transform);
                        break;
                    case 6:
                        Instantiate(powerPalllet, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.identity, pallet.transform);
                        break;
                    case 7:
                        if(fullMap[i - 1, j] == 0 && fullMap[i, j - 1] == 7)
                            Instantiate(tRight, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.identity, wall.transform);
                        else if(fullMap[i + 1, j] == 0 && fullMap[i, j - 1] == 7)
                            Instantiate(tLeft, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 180)), wall.transform);
                        else if(fullMap[i + 1, j] == 0 && fullMap[i, j + 1] == 7)
                            Instantiate(tRight, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.Euler(new Vector3(0, 0, 180)), wall.transform);
                        else
                            Instantiate(tLeft, new Vector3((j - 1) * 1.28f, (fullMap.GetLength(0) - i - 2) * 1.28f, 0f), Quaternion.identity, wall.transform);
                        break;        
                }  
            }
        }

    }

    bool IsLine(int i, int j)
    {       
        if (fullMap[i, j] == 1 || fullMap[i, j] == 2 || fullMap[i, j] == 3 || fullMap[i, j] == 4 || fullMap[i, j] == 7)
            return true;
        return false;
    }

    void InitiateCharacters()
    {
        GameObject spidy = GameObject.FindGameObjectWithTag ("Spidy");
        blueJack = Instantiate(blueJack, new Vector3(1 * 1.28f, 27 * 1.28f, 0f), Quaternion.identity);
        Instantiate(spidy1, new Vector3(11 * 1.28f, 15 * 1.28f, -0.2f), Quaternion.identity, spidy.transform);
        Instantiate(spidy2, new Vector3(11 * 1.28f, 13 * 1.28f, -0.2f), Quaternion.identity, spidy.transform);
        Instantiate(spidy3, new Vector3(16 * 1.28f, 15 * 1.28f, -0.2f), Quaternion.identity, spidy.transform);
        Instantiate(spidy4, new Vector3(16 * 1.28f, 13 * 1.28f, -0.2f), Quaternion.identity, spidy.transform);
    }

    IEnumerator InstantiateCherry()
    {
         while(true)
         {
            yield return new WaitForSeconds(30f);
            int edge = Random.Range(1, 5);
            int grid = Random.Range(0, 29);
            if(edge == 1)
            {
                newBonus = Instantiate(bonus, new Vector3(-1 * 1.28f, grid * 1.28f, 0f), Quaternion.identity);
            }
            else if(edge == 2)
            {
                newBonus = Instantiate(bonus, new Vector3(28 * 1.28f, grid * 1.28f, 0f), Quaternion.identity);
            }
            else if(edge == 3)
            {
                newBonus = Instantiate(bonus, new Vector3(grid * 1.28f, -1 * 1.28f, 0f), Quaternion.identity);
            }
            else
            {
                newBonus = Instantiate(bonus, new Vector3(grid * 1.28f, 29 * 1.28f, 0f), Quaternion.identity);
            }
         }
    }

    IEnumerator CountDown()
    {
        countDown.text = "3";
        yield return new WaitForSeconds(1f);
        countDown.text = "2";
        yield return new WaitForSeconds(1f);
        countDown.text = "1";
        yield return new WaitForSeconds(1f);
        countDown.text = " GO!";
        yield return new WaitForSeconds(1f);
        countDown.text = "";
        start = true;
        bgSource.loop = true;
        bgSource.Play();
        Time.timeScale = 1f;
        GameObject.FindWithTag("BlueJack").GetComponent<Animator>().enabled = true;
        GameObject.FindWithTag("BlueJack").GetComponent<PacStudentController>().start = true;
        GameObject.FindWithTag("Spidy1").GetComponent<Animator>().enabled = true;
        GameObject.FindWithTag("Spidy1").GetComponent<SpidyController>().start = true;
        GameObject.FindWithTag("Spidy2").GetComponent<Animator>().enabled = true;
        GameObject.FindWithTag("Spidy2").GetComponent<SpidyController>().start = true;
        GameObject.FindWithTag("Spidy3").GetComponent<Animator>().enabled = true;
        GameObject.FindWithTag("Spidy3").GetComponent<SpidyController>().start = true;
        GameObject.FindWithTag("Spidy4").GetComponent<Animator>().enabled = true;
        GameObject.FindWithTag("Spidy4").GetComponent<SpidyController>().start = true;

        StartCoroutine(InstantiateCherry());
    }

    public void ChangeBackgroundMusic(int bgNum)
    {
            bgSource.Stop();
            bgSource = gameObject.GetComponents<AudioSource>()[bgNum];
            if(bgNum == 2)
            {
                spidyDead = false;
                bgSource.loop = false;
            }
            else
            {
                bgSource.loop = true;
            }
            bgSource.Play();
    }
    

}
